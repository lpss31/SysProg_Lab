#include "block.h"

block_header_t* block_split(block_header_t* block, size_t requested_size) {
    size_t remaining_size = block->size - requested_size - sizeof(block_header_t);

    if (remaining_size >= (2 * ALIGNMENT)) { // Ensure enough space for new block header
        block_header_t* new_block = block_next(block);
        new_block->size = remaining_size;
        new_block->prev_size = block->size;
        new_block->is_busy = false;
        new_block->is_first = block->is_first;
        new_block->is_last = block->is_last ? false : block_next(block)->is_last;

        block->size = requested_size;
        block->is_last = true;

        return new_block;
    }

    return NULL;
}

void block_merge(block_header_t* block) {
    if (block->is_last) {
        return;
    }

    block_header_t* next_block = block_next(block);
    if (!next_block->is_busy) {
        block->size += next_block->size + sizeof(block_header_t);
        block->is_last = next_block->is_last;
    }
}

size_t block_get_size(block_header_t* block) {
    return block->size;
}

void block_set_size(block_header_t* block, size_t size) {
    block->size = size;
}

size_t block_get_prev_size(block_header_t* block) {
    return block->prev_size;
}

void block_set_prev_size(block_header_t* block, size_t size) {
    block->prev_size = size;
}

bool block_is_busy(block_header_t* block) {
    return block->is_busy;
}

void block_set_busy(block_header_t* block, bool is_busy) {
    block->is_busy = is_busy;
}

bool block_is_first(block_header_t* block) {
    return block->is_first;
}

void block_set_first(block_header_t* block, bool is_first) {
    block->is_first = is_first;
}

bool block_is_last(block_header_t* block) {
    return block->is_last;
}

void block_set_last(block_t* block, bool is_last) { 
    block->is_last = is_last;
}

