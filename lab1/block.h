#ifndef BLOCK_H
#define BLOCK_H

#include <stdint.h>

typedef struct block_header {
    size_t size;                // Current block size
    size_t prev_size;           // Size of the previous block
    uint8_t is_busy : 1;        // Flag indicating if block is busy
    uint8_t is_first : 1;       // Flag indicating if block is first in arena
    uint8_t is_last : 1;        // Flag indicating if block is last in arena
    uint8_t padding : 5;        // Padding for alignment
} block_header_t;

static inline void* block_to_payload(block_header_t* block) {
    return (void*)((uintptr_t)block + sizeof(block_header_t));
}

static inline block_header_t* payload_to_block(void* payload) {
    return (block_header_t*)((uintptr_t)payload - sizeof(block_header_t));
}

static inline block_header_t* block_next(block_header_t* block) {
    return (block_header_t*)((uintptr_t)block + block->size);
}

static inline block_header_t* block_prev(block_header_t* block) {
    return (block_header_t*)((uintptr_t)block - block->prev_size);
}

size_t block_get_size(block_header_t* block) {
    return block->size;
}

void block_set_size(block_header_t* block, size_t size) {
    block->size = size;
}

size_t block_get_prev_size(block_header_t* block) {
    return block->prev_size;
}

void block_set_prev_size(block_header_t* block, size_t size) {
    block->prev_size = size;
}

bool block_is_busy(block_header_t* block) {
    return block->is_busy;
}

void block_set_busy(block_header_t* block, bool is_busy) {
    block->is_busy = is_busy;
}

bool block_is_first(block_header_t* block) {
    return block->is_first;
}

void block_set_first(block_header_t* block, bool is_first) {
    block->is_first = is_first;
}

bool block_is_last(block_header_t* block) {
    return block->is_last;
}

void block_set_last(block_header_t* block, bool is_last) {
    block->is_last = is_last;
}

#endif

