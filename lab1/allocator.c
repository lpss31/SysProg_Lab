#include "allocator.h"
#include <stdint.h>
#include <stdlib.h> // For initial arena allocation
#include <stdio.h>  // For mem_show

#define ARENA_START ((block_header_t*)DEFAULT_ARENA_SIZE)  // Cast to block_header_t

static block_header_t* arena_start = NULL;

void* mem_alloc(size_t size) {
    // Ensure size is aligned
    size_t aligned_size = (size + (ALIGNMENT - 1)) & ~(ALIGNMENT - 1);

    // Check for initial arena allocation
    if (arena_start == NULL) {
        arena_start = (block_header_t*)malloc(DEFAULT_ARENA_SIZE);
        if (arena_start == NULL) {
            return NULL; // Memory allocation failed
        }
        arena_start->size = DEFAULT_ARENA_SIZE - sizeof(block_header_t);
        arena_start->prev_size = 0;
        arena_start->is_busy = false;
        arena_start->is_first = true;
        arena_start->is_last = true;
    }

    // Search for a free block with enough size
    block_header_t* current_block = arena_start;
    while (current_block) {
        if (!current_block->is_busy && current_block->size >= aligned_size) {
            // Found a suitable free block
            if (current_block->size > aligned_size + sizeof(block_header_t)) {
                // Split the block if there's enough space for another block
                block_header_t* new_block = block_split(current_block, aligned_size);
                return block_to_payload(new_block);
            } else {
                // Use the entire block
                current_block->is_busy = true;
                return block_to_payload(current_block);
            }
        }
        current_block = block_next(current_block);
    }

    // No free block found
    return NULL;
}

