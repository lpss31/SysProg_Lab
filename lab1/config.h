#ifndef CONFIG_H
#define CONFIG_H

#define PAGE_SIZE 4096  // Adjust as needed (must be power of 2)
#define DEFAULT_ARENA_SIZE (10 * PAGE_SIZE)  // Adjust as needed

#endif

