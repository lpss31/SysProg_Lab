#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include "block.h"

void* mem_alloc(size_t size);
void mem_free(void* ptr);
void mem_show();

#endif

