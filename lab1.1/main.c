#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define BLOCK_SIZE 4096
#define WORD_SIZE sizeof(void*)
#define ALIGNMENT (WORD_SIZE - 1)
#define ALIGN(size) (((size) + ALIGNMENT) & ~ALIGNMENT)

typedef struct block {
    size_t size;
    struct block* next;
    struct block* prev;
    char buffer[];
} block_t;

static block_t* head = NULL;
static block_t* free_list = NULL;

void* my_malloc(size_t size);
void my_free(void* ptr);
void* my_calloc(size_t nmemb, size_t size);
void* my_realloc(void* ptr, size_t size);
void mem_show(void);

void* my_malloc(size_t size) {
    block_t* block;
    block_t* prev;
    size_t aligned_size = ALIGN(size);

    if (aligned_size == 0) {
        return NULL;
    }

    for (block = free_list, prev = NULL; block != NULL; prev = block, block = block->next) {
        if (block->size >= aligned_size) {
            if (block->size - aligned_size >= sizeof(block_t) + WORD_SIZE) {
                block->size -= aligned_size + WORD_SIZE;
                block = (block_t*)((char*)block + block->size);
                block->size = aligned_size;
                block->next = free_list;
                block->prev = prev;
                if (prev != NULL) {
                    prev->next = block;
                } else {
                    free_list = block;
                }
                return (void*)((char*)block + WORD_SIZE);
            } else {
                if (prev != NULL) {
                    prev->next = block->next;
                } else {
                    free_list = block->next;
                }
                block->size = aligned_size;
                return (void*)((char*)block + WORD_SIZE);
            }
        }
    }

    block = (block_t*)malloc(sizeof(block_t) + aligned_size);
    if (block == NULL) {
        return NULL;
    }
    block->size = aligned_size;
    block->next = free_list;
    block->prev = NULL;
    free_list = block;
    return (void*)((char*)block + WORD_SIZE);
}

void my_free(void* ptr) {
    block_t* block;

    if (ptr == NULL) {
        return;
    }

    block = (block_t*)((char*)ptr - WORD_SIZE);
    block->next = free_list;
    block->prev = NULL;
    free_list = block;
}

void* my_calloc(size_t nmemb, size_t size) {
    size_t total_size = nmemb * size;
    void* ptr = my_malloc(total_size);

    if (ptr != NULL) {
        memset(ptr, 0, total_size);
    }

    return ptr;
}

void* my_realloc(void* ptr, size_t size) {
    block_t* block;
    void* new_ptr;
    size_t old_size;

    if (ptr == NULL) {
        return my_malloc(size);
    }

    block = (block_t*)((char*)ptr - WORD_SIZE);
    old_size = block->size;

    if (old_size >= size) {
        return ptr;
    }

    new_ptr = my_malloc(size);
    if (new_ptr == NULL) {
        return NULL;
   }

    memcpy(new_ptr, ptr, old_size);
    my_free(ptr);
    return new_ptr;
}

void mem_show(void) {
    block_t* block;
    size_t total_allocated = 0;
    size_t total_free = 0;

    printf("Memory blocks:\n");
    for (block = head; block != NULL; block = block->next) {
        printf("Block at %p, size %u, free %u\n", block, block->size, block->size - sizeof(block_t));
        total_allocated += block->size;
        if (block->size > sizeof(block_t)) {
            total_free += block->size - sizeof(block_t);
        }
    }

    printf("Total allocated: %u\n", total_allocated);
    printf("Total free: %u\n", total_free);
}

struct T {
    void *ptr;
    size_t size;
    unsigned int checksum;
};

static unsigned int
buf_checksum(const unsigned char *c, size_t size)
{
    unsigned int checksum = 0;

    while (size--)
        checksum = (checksum << 3) ^ (checksum >> 5) ^ *c++;
    return checksum;
}

static void
buf_fill(unsigned char *c, size_t size)
{
    while (size--)
        *c++ = (unsigned char)rand();
}

static void *
buf_alloc(size_t size)
{
    void *ptr;

    ptr = my_malloc(size);
    if (ptr != NULL)
        buf_fill(ptr, size);
    return ptr;
}

static void
tester(const bool verbose)
{
    const size_t t_NUM = 100;
    const size_t SZ_MIN = 1;
    const size_t SZ_MAX = 4094 * 10;
    unsigned long M = 1000;
    struct T t[t_NUM];
    void *ptr;
    size_t idx, size, size_min;
    unsigned int checksum;

    for (idx = 0; idx < t_NUM; ++idx)
        t[idx].ptr = NULL;
    for (unsigned long i = 0; i < M; ++i) {
        if (verbose)
            mem_show("------------------------");
        for (idx = 0; idx < t_NUM; ++idx) {
            if (t[idx].ptr != NULL)
                if (buf_checksum(t[idx].ptr, t[idx].size) != t[idx].checksum) {
                    printf("1. Checksum failed at [%p]\n", t[idx].ptr);
                    return;
                }
        }
        idx = (size_t)rand() % t_NUM;
        if (t[idx].ptr == NULL) {
            size = ((size_t)rand() % (SZ_MAX - SZ_MIN)) + SZ_MIN;
            if (verbose)
                printf("ALLOC %zu ", size);
            ptr = buf_alloc(size);
            if (ptr != NULL) {
                t[idx].size = size;
                t[idx].ptr = ptr;
                t[idx].checksum = buf_checksum(ptr, size);
                if (verbose)
                    printf("-> [%p]\n", ptr);
            } else {
                if (verbose)
                    printf("failed\n");
            }
        } else if (rand() & 1) {
            if (verbose)
                printf("REALLOC [%p] %zu ", t[idx].ptr, t[idx].size);
            size = ((size_t)rand() % (SZ_MAX - SZ_MIN)) + SZ_MIN;
            size_min = size < t[idx].size ? size : t[idx].size;
            checksum = buf_checksum(t[idx].ptr, size_min);
            ptr = my_realloc(t[idx].ptr, size);
            if (ptr != NULL) {
                if (verbose)
                    printf("-> [%p] %zu\n", ptr, size);
                if (checksum != buf_checksum(ptr, size_min)) {
                    printf("2. Checksum failed at [%p]\n", ptr);
                    return;
                }
                buf_fill(ptr, size);
                t[idx].ptr = ptr;
                t[idx].size = size;
                t[idx].checksum = buf_checksum(t[idx].ptr, t[idx].size);
            } else {
                if (verbose)
                    printf("failed\n");
            }
        } else {
            if (verbose)
                printf("FREE [%p]\n", t[idx].ptr);
            my_free(t[idx].ptr);
            t[idx].ptr = NULL;
        }
    }
    for (idx = 0; idx < t_NUM; ++idx) {
        if (t[idx].ptr != NULL)
            if (buf_checksum(t[idx].ptr, t[idx].size) != t[idx].checksum) {
                printf("3. Checksum failed at [%p]\n", t[idx].ptr);
                return;
            }
        my_free(t[idx].ptr);
    }
    if (verbose)
        mem_show("------------------------");
}


int main(void) {
    int* p1 = (int*)my_malloc(10 * sizeof(int));
    int* p2 = (int*)my_calloc(10, sizeof(int));
    int* p3 = (int*)my_realloc(p1, 20 * sizeof(int));
    mem_show();
    my_free(p1);
    my_free(p2);
    my_free(p3);
    mem_show();
    return 0;
}
